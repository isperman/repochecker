Repository Checker
==================

This little script allows to detect uncommitted and unpushed changes
in local **git** repositories. The script checks the given repositories
for changes on all branches. This script does not change anything.
It will not pull, merge, push or commit anything. This is something
the user should do actively.

# Requirements

* Tell the name of the repositories which have uncommitted files
* Tell the name of the repositories which have unpushed commits
* Integration in the task bar of the OS (Ubuntu, later macOS)
* An installer to install the script in the OS (Ubuntu, later macOS)

# Implementation Details

## Language

The application is implemented in python3, allowing a wide portability.

## List of repositories
The paths to the repositories to check is in `~/.checkthis`.
Every line corresponds to a repository to check.
Optionally, the name of one remote can be provided, to check whether
the all commits have been pushed, separated by a semicolon.
Further, some branches can be excluded from the check, my adding them
preceded by a tile. Example:
`~/myRepo23;remote;~testBranch,~branchxy`.


We do not want to automatically include every git repository in the home
repository, but there is the possibility to automatically generate the
`~/.checkthis` file from our application. The user can then remove the
unwanted repositories from the list.

## Performed Actions
This script performs a `git status` in every repository.

## OS Integration
An icon with text is shown in the taskbar.
If there are no uncommitted changes, the icon is green, otherwise
the icon is green.
If everything is committed but not every commit was pushed the icon
is orange.

## Installation
The installer script `install.sh` copies the executable `gitc` to
 `/usr/local/bin/` such that it is callable from anywhere in the system.

## Executable
The executable is a bash script called `gitc`.

TODO: document what this wrapper does.

# References  
[AstroFloyd's blog: gitcheck ](https://astrofloyd.wordpress.com/2013/02/10/gitcheck-check-all-your-git-repositories-for-changes/)
