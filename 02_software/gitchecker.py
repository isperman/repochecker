import subprocess
import os
from enum import Enum
import logging


class GitChecker:
    
    _modifiedFiles = None
    _untrackedFiles = None

    def __init__(self, dir):
        """
        It does initialize the branches and the names of the remote but does not polulate them.
        It checks whether in the given location there is a git repository.
        It does not yet check whether the repository has changes or not.
        :param dir: location of the git repository
        """
        self.dir = os.path.expanduser(dir)
        self.name = dir
        self.remoteNames = list()
        self.branches = list()

        self.checkIfGitRepository()

    def get_modified_files(self):
        return self._modifiedFiles

    def get_untracked_files(self):
        return self._untrackedFiles

    def checkRepository(self):
        """
        This function checks is the git repository has changes.
        :return: It returns EVERYTHING_OK if no changes are detected, an other code otherwise.
        """
        self.checkIfGitRepository()
        # print(self.dir)
        self.populateModifiedFiles()
        # self.populateUntrackedFiles()
        if len(self.modifiedFiles) > 0:
            return GitCheckerStatus.HAS_MODIFIED
        if self.is_there_an_untracked_file():
            return GitCheckerStatus.HAS_UNTRACKED
        elif self.is_something_unpushed():
            return GitCheckerStatus.HAS_UNPUSHED
        else:
            return GitCheckerStatus.EVERYTHING_OK

    def switchDir(self):
        """
        Switches to the git repository
        :return:
        """
        os.chdir(self.dir)
        # subprocess.run(["cd", self.dir])

    def populateModifiedFiles(self):
        """
        Checks if there are modified files in the repository. It adds the name of them in self.modifiedFiles
        :return:
        """
        self.switchDir()
        out = subprocess.run(["git", "ls-files", "-m"], stdout=subprocess.PIPE)
        out = out.stdout.decode("utf-8")
        self.modifiedFiles = self.commandOutputToFileList(out)
        # print(self.modifiedFiles)

    def is_there_an_untracked_file(self):
        """
        Checks if the repository contains untracked files.
        :return: True if there are untracked files, False otherwise.
        """
        self.switchDir()
        out = subprocess.run(["git", "ls-files", "--others", "--exclude-standard"], stdout=subprocess.PIPE).stdout.decode("utf-8")
        logging.debug("Untracked file(s) in {}: \n {}".format(self.name,out))
        if(len(out)>0):
            return True

    def is_something_unpushed(self):
        """
        Determined if there are unpushed commits.
        It does so by checking the logs between branch and remote/branch
        :return: True if there are unpushed changes, False otherwise.
        """
        for branch in self.branches:
            out = subprocess.run(["git", "log", "{0}/{1}..{1}".format(self.remoteNames[0], branch)], stdout=subprocess.PIPE).stdout.decode("utf-8")
            #logging.debug(out)
            if(len(out) > 0):
                return True

    def is_something_staged_for_commit(self):
        # TODO: write code for this one
        pass


    def checkIfGitRepository(self):
        """
        This method checks whether this repository is acutally a git repository.
        It throws a TypeError exception if it's not a git repository.
        :return:
        """
        # git - C < path > rev - parse
        try:
            self.switchDir()
        except FileNotFoundError:
            raise RuntimeError("This directory does not exist: {}".format(self.dir))
            pass
        else:
            out = subprocess.run(["git", "-C", self.dir , "rev-parse"], stdout=subprocess.PIPE, stderr=subprocess.PIPE).stderr.decode("utf-8")
            if len(out) > 0:
                raise TypeError("Repository {} is not under git".format(self.dir))


    @staticmethod
    def commandOutputToFileList(commandOutput):
        """
        Splits the output of git ls-files into the individual file names
        :param commandOutput: Command output of a list of several file names (like git ls-files)
        :return: list of the individual file names
        """
        return commandOutput.splitlines()


class GitCheckerStatus(Enum):
    EVERYTHING_OK = 0
    HAS_MODIFIED = 1
    HAS_UNPUSHED = 2
    HAS_UNTRACKED = 3
    HAS_STAGED = 4
