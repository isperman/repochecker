from gitchecker import GitChecker
import os
import logging

class Configuration:
    """
    Contains the configuration of the application.
    Contains the list of all desired git repositories in the variable repositories
    """
    # Name of the configuration file
    confPath = None

    def __init__(self, filename=None):
        """
        Initializes the list of git repositories.
        """
        self.repositories = list()
        # self.read_configuration(filename)

    def read_configuration(self, filename):
        """
        Loads all repositories from the configuration file.
        Populates (indirectly) self.repositories with the information found in the configuration file.
        :param filename: Location of the configuration file
        :return:
        """
        self.confPath = filename
        conf = open(os.path.expanduser(filename), "r")
        for line in conf:
            self.read_configuration_line(line)

    def read_configuration_line(self, config_line):
        """
        Reads one line of the configuration file. From that it generates a gitchecker (repository) object
        and appends it to self.repositories.
        :param config_line:
        :return:
        """
        data = config_line.split(";")
        repo = GitChecker(data[0].strip())
        logging.debug("adding repo {}".format(repo.name))
        repo.remoteNames.append(data[1].strip())

        branches = data[2].split(",")
        for branch in branches:
            repo.branches.append(branch.strip())

        self.repositories.append(repo)

    def get_repo_paths(self):
        """

        :return: A list of paths of all repositories found if self.repositories.
        """
        try:
            self.repositories
        except NameError:
            raise AttributeError("Object has to be initialized with read_configuration!")

        repos = list()
        for repo in self.repositories:
            repos.append(repo.name)

        return repos