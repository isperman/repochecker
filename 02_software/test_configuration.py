from unittest import TestCase
from configuration import Configuration


class TestConfiguration(TestCase):

    def setUp(self):
        pass

    def test_read_configuration_line(self):
        conf = Configuration()
        # print(conf)
        conf.read_configuration_line("~/repochecker/; origin; master,dev ")
        self.assertEqual(conf.repositories[0].location,"~/repochecker/")

    def test_read_configuration_line2(self):
        conf2 = Configuration()
        # print(conf2)
        conf2.read_configuration_line("~/asd/asd2/; origin; master")

        self.assertEqual(conf2.repositories[0].location, "~/asd/asd2/")
        self.assertEqual(conf2.repositories[0].remoteNames[0], "origin")
        self.assertEqual(conf2.repositories[0].branches[0], "master")