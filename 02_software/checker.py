import argparse
import subprocess
import os
import warnings
from gitchecker import GitChecker, GitCheckerStatus
from configuration import Configuration
import logging

# Parse command line arguments
parser = argparse.ArgumentParser(description='Check for changes in your local igt repositories')
parser.add_argument('--d',
                    dest='debug',
                    action="store_true",
                    help='Shows additional debug information'
                    )
args = parser.parse_args()

# Set logging level according to the given argument
if(args.debug):
    logging.basicConfig(level=logging.DEBUG)
else:
    logging.basicConfig(level=logging.WARNING)

# Generate the configuration
conf = Configuration()
try:
    conf.read_configuration("~/.checkthis")
except RuntimeError as e:
    print("Problems, only problems\n{}".format(e))
    exit()

# Get the list of git repositories (gitchecker objects)
repos = conf.repositories

# Print all the repositories for debug purposes
logging.debug("Repositories to check:\n {}".format(conf.get_repo_paths()))

# For every git repository, check its status
for repo in repos:
    st = repo.checkRepository()
    if(st == GitCheckerStatus.HAS_MODIFIED):
        print("repo {} has modified files".format(repo.name))
    elif st is GitCheckerStatus.HAS_UNPUSHED:
        print("repo {} has unpushed commits".format(repo.name))
    elif st is GitCheckerStatus.HAS_UNTRACKED:
        print("repo {} has untracked files".format(repo.name))
    else:
        logging.info("Repo {} is OK".format(repo.name))
