from unittest import TestCase
from gitchecker import GitChecker

class TestGitChecker(TestCase):
    def test_commandOutputToFileList(self):
        data = "file0\nfile1\nfile2"
        out = GitChecker.commandOutputToFileList(data)

        desiredOut = list()
        desiredOut.append("file0")
        desiredOut.append("file1")
        desiredOut.append("file2")

        self.assertEqual(out, desiredOut)

        out1 = GitChecker.commandOutputToFileList("asd\n123")
        self.assertEqual(out1, ["asd", "123"])

        out2 = GitChecker.commandOutputToFileList("mhz\n345\n")
        self.assertEqual(out2, ["mhz", "345"])

        # with real data

        real_data = "02_software/checker.py\n02_software/configuration.py\n02_software/gitchecker.py"
        real_out = GitChecker.commandOutputToFileList(real_data)
        self.assertEqual(real_out[0], "02_software/checker.py")
        self.assertEqual(real_out[1], "02_software/configuration.py")
        self.assertEqual(real_out[2], "02_software/gitchecker.py")
